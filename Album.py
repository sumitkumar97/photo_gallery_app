from peewee import *
from User import User

db = SqliteDatabase('gallery.db')

class Album(Model):
    album_id = AutoField()
    description = CharField()
    cover_photo = BlobField()
    #List<photos> iterate through photos
    visibility = BooleanField()
    user_id =  ForeignKeyField(User, field='user_id')
    #location
    created_at = DateField(default=datetime.datetime.now)

    class Meta:
        database = db
