from peewee import *
from Album import Album
from User import User

db = SqliteDatabase('gallery.db')

class LikedAlbums(Model):
    album_id = ForeignKeyField(Album, field='album_id')
    user_id = ForeignKeyField(User, field='user_id')

    class Meta:
        database = db
