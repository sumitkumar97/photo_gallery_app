from peewee import *

db = SqliteDatabase('gallery.db')

class User(Model):
    user_id = AutoField()
    username = CharField() #const
    first_name = CharField()
    last_name = CharField()
    email = CharField()
    gender = CharField()
    profile_picture = BlobField()

    class Meta:
        database = db
