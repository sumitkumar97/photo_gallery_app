from peewee import *
from Album import Album

db = SqliteDatabase('gallery.db')

class Photo(Model):
    photo_id = AutoField()
    album_id = ForeignKeyField(Album, field='album_id')
    created_at = DateField(default=datetime.datetime.now)
    visibility = BooleanField()
    #list<UserId> liked by
    #location
    
    class Meta:
        database = db
