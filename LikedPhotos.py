from peewee import *
from Photo import Photo
from User import User

db = SqliteDatabase('gallery.db')

class LikedPhotos(Model):
    photo_id = ForeignKeyField(Photo, field='photo_id')
    user_id = ForeignKeyField(User, field='user_id')

    class Meta:
        database = db
